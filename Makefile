CURRENT_DIR=$(shell pwd)


IMAGE_NAME := zoo
SERVER_NAME := zoo

all: build lint test

build: generate-pb lint go

ci: lint-ci generate-pb publish-pb build-ci test-ci

# update go.mod to reference generated protobuf code from build dir rather than remote repo
pb-local:
	go mod edit -replace bitbucket.org/benhschwartz/protorepo-zoo-go=$(CURRENT_DIR)/build/pb/zoo/go

# update go.mod to reference generated protobuf code from remote repo rather than local build
pb-remote:
	go mod edit -dropreplace bitbucket.org/benhschwartz/protorepo-zoo-go


lint:
	go vet ./...
	go fmt ./...

go:
	GOOS=linux GOARCH=amd64 go build -o ./$(SERVER_NAME)-linux-amd64 ./cmd/server/main.go

unit:
	go test ./...

test:
	# ensure db is available for integration tests
	PGPASSWORD=docker psql -h "localhost" -p 5432 -U "docker" -c '\q' \
		|| (echo ">> Run 'docker-compose up' to start integration env for tests" && exit 1)

	go clean -testcache
	TESTDB_DSN=postgres://docker:docker@localhost:5432/hippos_test?sslmode=disable \
		MIGRATION_DIR=$(PWD)/migrations \
		go test -tags=integration ./...

lint-ci:
	# ensure go.mod isn't edited for local pb dev
	if grep replace ./go.mod ; then echo ">> go.mod shouldn't contain replace statement" && exit 1 ; fi

generate-pb:
	./config/scripts/generate.sh

publish-pb:
	./config/scripts/publish.sh

build-ci:
	docker build --build-arg servername=$(SERVER_NAME) -t $(IMAGE_NAME) -f config/Dockerfile .

test-ci:
	docker-compose -f docker-compose-ci.yml up --build --abort-on-container-exit
	docker-compose -f docker-compose-ci.yml down --volumes


docs:
	mkdir -p build/docs
	docker run --rm \
    	-v $(CURRENT_DIR)/build/docs/:/out \
    	-v $(CURRENT_DIR)/api/zoo/:/protos \
    	pseudomuto/protoc-gen-doc

clean:
	rm -rf build

.PHONY: build

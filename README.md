
# Dev Workflow

    # checkout the project
    git clone git@bitbucket.org:benhschwartz/ms-poc.git
    cd ms-poc 

    # start db server
    docker-compose up -d postgres
    
    # test, build, and start server
    make
    docker-compose up -d zoo

    # (make some changes)

    # re-test, re-build, and restart
    make
    docker-compose restart zoo

    # run fully hermetic build/test before pushing changes
    make ci

    git commit -am "all the changes"
    git push origin my-branch

    # cleanup
    docker-compose down

# Other Build Options

## build/test/run on host

    # start db server
    docker-compose up -d postgres

    # build/test
    make

    # run server
	DATABASE_DSN=postgres://docker:docker@localhost:5432/hippos?sslmode=disable \
	    MIGRATION_DSN=postgres://docker:docker@localhost:5432/hippos?sslmode=disable \
	    MIGRATION_DIR=$(pwd)/migrations \
        ./zoo
    
    # run demo client integration
    go run cmd/client_demo/main.go

    # cleanup
    docker-compose down

## build/test with docker-compose

    # build docker image, run all tests in docker against docker-compose integration environment
    make ci

## build locally, run with docker-compose

    # build/test
    make build

    # start up postgres & the zoo server
    docker-compose up -d

    # run demo client integration
    go run cmd/client_demo/main.go

    # cleanup
    docker-compose down

    

## visibility

http://localhost:5200/metrics

http://localhost:5000/robots.txt

## migrations
(https://lanre.wtf/blog/2019/01/02/database-migration-golang/)

get the migrate tool to create new migrations (install to gopath, not in project):

    go get -tags 'postgres' -u github.com/golang-migrate/migrate/cmd/migrate
    
    
create new migrations sql

    migrate create -ext sql create_hippos_table


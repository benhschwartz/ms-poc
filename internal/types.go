package internal

import (
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
)

type ZooDal interface {
	Find(id uint64) (*zoo.Hippo, error)
	Create(hippo *zoo.Hippo) (*zoo.Hippo, error)
}

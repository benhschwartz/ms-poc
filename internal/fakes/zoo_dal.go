package fakes

import (
	"bitbucket.org/benhschwartz/ms-poc/internal"
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"errors"
)

// verify we are following the interface
var _ internal.ZooDal = &ZooDal{}

type ZooDal struct {
	i      uint64
	hippos []*zoo.Hippo
}

func NewZooDal() *ZooDal {

	return &ZooDal{i: 0, hippos: []*zoo.Hippo{}}
}

func (r *ZooDal) Find(id uint64) (*zoo.Hippo, error) {
	for _, hippo := range r.hippos {
		if hippo.Id == id {
			return hippo, nil
		}
	}
	return nil, errors.New("not found")
}

func (r *ZooDal) Create(hippo *zoo.Hippo) (*zoo.Hippo, error) {
	r.i = r.i + 1
	hippo.Id = r.i
	r.hippos = append(r.hippos, hippo)
	return hippo, nil
}

package arandom

import (
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"math/rand"
	"time"
)

var names []string

func init() {
	rand.Seed(time.Now().UnixNano())
	names = []string{"Aaran", "Aaren", "Aarez", "Aarman", "Aaron", "Aaron-James", "Aarron", "Aaryan", "Aaryn", "Aayan", "Aazaan"}
}

func Hippo() *zoo.Hippo {
	return &zoo.Hippo{
		Name:   names[rand.Intn(len(names))],
		Weight: uint64(rand.Intn(10000)),
	}
}

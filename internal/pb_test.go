package internal

import (
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_ZooServer_GetHippo(t *testing.T) {
	// given
	hippo := &zoo.Hippo{
		Name:   "Bob Loblaw",
		Weight: 3000,
		//Desc:   "grey",
	}

	// then
	assert.Equal(t, "Bob Loblaw", hippo.GetName())
	//assert.Equal(t, "grey", hippo.GetDesc())
}

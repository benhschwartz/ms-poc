package app

import (
	"bitbucket.org/benhschwartz/ms-poc/internal/fakes"
	"bitbucket.org/benhschwartz/ms-poc/internal/fakes/arandom"
	serviceFakes "bitbucket.org/benhschwartz/ms-poc/pkg/fakes"
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"context"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

// Setup gRPC server for tests
func GetTestConn() *grpc.ClientConn {
	// wire grpc server with dependencies
	zooDal := &fakes.ZooDal{}
	s := NewZooServer(zooDal)

	grpcServer := grpc.NewServer()
	zoo.RegisterZooServer(grpcServer, s)

	// start grpc server
	ctx := context.Background()
	return serviceFakes.GetFakeGRPCConnection(ctx, serviceFakes.FakeGRPCDialer(grpcServer))
}

func Test_ZooServer_GetHippo(t *testing.T) {
	// given
	testConn := GetTestConn()
	defer testConn.Close()
	ctx := context.Background()
	client := zoo.NewZooClient(testConn)
	newHippo := arandom.Hippo()
	req := &zoo.CreateHippoRequest{Hippo: newHippo}

	// when
	createdHippo, err := client.CreateHippo(ctx, req)

	// then
	assert.Nil(t, err)
	er, _ := status.FromError(err)
	assert.Equal(t, codes.OK, er.Code())
	assert.Equal(t, newHippo.GetName(), createdHippo.GetName())
	assert.Equal(t, newHippo.GetWeight(), createdHippo.GetWeight())
}

func Test_ZooServer_CreateHippo(t *testing.T) {
	// given
	testConn := GetTestConn()
	defer testConn.Close()
	ctx := context.Background()
	client := zoo.NewZooClient(testConn)
	expected, err := client.CreateHippo(ctx, &zoo.CreateHippoRequest{
		Hippo: arandom.Hippo(),
	})

	// when
	found, err := client.GetHippo(ctx, &zoo.GetHippoRequest{Id: expected.GetId()})

	// then
	assert.Nil(t, err)
	er, _ := status.FromError(err)
	assert.Equal(t, codes.OK, er.Code())
	assert.Equal(t, expected.GetId(), found.GetId())
	assert.Equal(t, expected.GetName(), found.GetName())
	assert.Equal(t, expected.GetWeight(), found.GetWeight())
}

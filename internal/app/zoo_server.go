package app

import (
	"bitbucket.org/benhschwartz/ms-poc/internal"
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"golang.org/x/net/context"
	"log"
)

// verify we are following the interface
var _ zoo.ZooServer = &ZooServer{}

type ZooServer struct {
	dal internal.ZooDal
}

func NewZooServer(dal internal.ZooDal) *ZooServer {
	return &ZooServer{dal: dal}
}

func (s *ZooServer) CreateHippo(ctx context.Context, req *zoo.CreateHippoRequest) (*zoo.Hippo, error) {
	log.Printf("Creatinggggggg Hippo: %v", req.GetHippo())
	return s.dal.Create(req.GetHippo())
}

func (s *ZooServer) GetHippo(ctx context.Context, req *zoo.GetHippoRequest) (*zoo.Hippo, error) {
	log.Printf("Getting Hippo: %v", req.GetId())
	return s.dal.Find(req.GetId())
}

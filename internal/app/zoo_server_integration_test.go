// +build integration

package app

import (
	"bitbucket.org/benhschwartz/migrate/pkg/migrations"
	"bitbucket.org/benhschwartz/ms-poc/internal/dal"
	"bitbucket.org/benhschwartz/ms-poc/internal/fakes/arandom"
	serviceFakes "bitbucket.org/benhschwartz/ms-poc/pkg/fakes"
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"context"
	"database/sql"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"os"
	"testing"
)

var testConn *grpc.ClientConn

// Setup db connection & gRPC server for tests
func TestMain(m *testing.M) {
	viper.AutomaticEnv()
	viper.SetDefault("MIGRATION_DIR", "/app/migrations")

	dsn := viper.GetString("TESTDB_DSN")
	migrationDir := viper.GetString("MIGRATION_DIR")

	db, err := sql.Open("pgx", dsn)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	if err := db.Ping(); err != nil {
		panic(err)
	}

	// migrate database
	if err := migrations.RunMigrations(db, migrationDir); err != nil {
		panic(err)
	}

	// wire grpc server with dependencies
	zooDal := &dal.ZooDal{Db: db}
	s := NewZooServer(zooDal)

	grpcServer := grpc.NewServer()
	zoo.RegisterZooServer(grpcServer, s)

	// start grpc server
	ctx := context.Background()
	testConn = serviceFakes.GetFakeGRPCConnection(ctx, serviceFakes.FakeGRPCDialer(grpcServer))
	defer testConn.Close()

	// run tests
	exitVal := m.Run()

	os.Exit(exitVal)
}

func TestInt_ZooServer_GetHippo(t *testing.T) {
	// given
	ctx := context.Background()
	client := zoo.NewZooClient(testConn)
	newHippo := arandom.Hippo()
	req := &zoo.CreateHippoRequest{Hippo: newHippo}

	// when
	createdHippo, err := client.CreateHippo(ctx, req)

	// then
	assert.Nil(t, err)
	er, _ := status.FromError(err)
	assert.Equal(t, codes.OK, er.Code())
	assert.Equal(t, newHippo.GetName(), createdHippo.GetName())
	assert.Equal(t, newHippo.GetWeight(), createdHippo.GetWeight())
}

func TestInt_ZooServer_CreateHippo(t *testing.T) {
	// given
	ctx := context.Background()
	client := zoo.NewZooClient(testConn)
	expected, err := client.CreateHippo(ctx, &zoo.CreateHippoRequest{
		Hippo: arandom.Hippo(),
	})

	// when
	found, err := client.GetHippo(ctx, &zoo.GetHippoRequest{Id: expected.GetId()})

	// then
	assert.Nil(t, err)
	er, _ := status.FromError(err)
	assert.Equal(t, codes.OK, er.Code())
	assert.Equal(t, expected.GetId(), found.GetId())
	assert.Equal(t, expected.GetName(), found.GetName())
	assert.Equal(t, expected.GetWeight(), found.GetWeight())
}

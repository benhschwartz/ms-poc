package dal

import (
	"bitbucket.org/benhschwartz/ms-poc/internal"
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"database/sql"
)

// verify we are following the interface
var _ internal.ZooDal = &ZooDal{}

type ZooDal struct {
	Db *sql.DB
}

func (d *ZooDal) Find(id uint64) (*zoo.Hippo, error) {
	var hippo zoo.Hippo
	sqlStatement := `select id, name, weight from hippos where id=$1`

	row := d.Db.QueryRow(sqlStatement, id)
	err := row.Scan(&hippo.Id, &hippo.Name, &hippo.Weight)

	return &hippo, err
}

func (d *ZooDal) Create(hippo *zoo.Hippo) (*zoo.Hippo, error) {
	sqlStatement := `insert into hippos(name, weight) values($1, $2) RETURNING id`

	var lastInsertId uint64
	err := d.Db.QueryRow(sqlStatement, hippo.GetName(), hippo.GetWeight()).Scan(&lastInsertId)
	if err != nil {
		return hippo, err
	}

	hippo.Id = lastInsertId
	return hippo, nil
}

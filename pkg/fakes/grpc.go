package fakes

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"net"
)

func GetFakeGRPCConnection(ctx context.Context, dialer func(context.Context, string) (net.Conn, error)) *grpc.ClientConn {
	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(), grpc.WithContextDialer(dialer))
	if err != nil {
		log.Fatal(err)
	}
	return conn
}

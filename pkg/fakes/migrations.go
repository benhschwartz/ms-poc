package fakes

import (
	"bitbucket.org/benhschwartz/migrate/pkg/migrations"
	"database/sql"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/pkg/errors"
)

type TestDb struct {
	DB           *sql.DB
	MigrationDir string
}

func NewTestDb(dsn string, migrationDir string) (*TestDb, error) {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}

	return &TestDb{DB: db, MigrationDir: migrationDir}, nil
}

func (d *TestDb) Close() {
	d.DB.Close()
}
func (d *TestDb) Up() error {
	if err := d.DB.Ping(); err != nil {
		return errors.Wrap(err, "could not ping database")
	}

	return migrations.RunMigrations(d.DB, d.MigrationDir)
}

func (d *TestDb) Truncate() error {
	stmt := "TRUNCATE TABLE list, item;"

	if _, err := d.DB.Exec(stmt); err != nil {
		return errors.Wrap(err, "couldn't truncate test database tables")
	}

	return nil

}

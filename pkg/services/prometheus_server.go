package services

import (
	oc_prometheus "contrib.go.opencensus.io/exporter/prometheus"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"go.opencensus.io/stats/view"
	"net/http"
)

func CreatePrometheusServer(portNumber int32, appId string) (*http.Server, error) {
	prometheusExporter, err := oc_prometheus.NewExporter(oc_prometheus.Options{
		Namespace: appId,
		Registry:  prometheus.DefaultRegisterer.(*prometheus.Registry),
	})
	if err != nil {
		return nil, err
	}

	view.RegisterExporter(prometheusExporter)

	mux := http.NewServeMux()

	mux.Handle("/metrics", prometheusExporter)

	server := http.Server{
		Addr:    fmt.Sprintf(":%d", portNumber),
		Handler: mux,
	}
	return &server, nil
}

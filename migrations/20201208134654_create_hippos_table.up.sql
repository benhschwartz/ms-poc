CREATE TABLE hippos (
  id SERIAL PRIMARY KEY,
  name varchar(40),
  weight integer
);

#!/usr/bin/env bash

set -e

BUILD_PATH="$(pwd)/build/pb"
REPO_PATH="$(pwd)/build/protolangs"

CURRENT_BRANCH="master"
#CURRENT_BRANCH=${CIRCLE_BRANCH-"branch-not-available"}


GIT_URL_BASE="git@bitbucket.org:benhschwartz"

# Helper for adding a directory to the stack and echoing the result
function enterDir {
	echo "Entering $1"
	pushd $1 > /dev/null
}

# Helper for popping a directory off the stack and echoing the result
function leaveDir {
	echo "Leaving `pwd`"
	popd > /dev/null
}

function setupBranch {
	enterDir $1

	echo "Creating branch"

	if ! git show-branch $CURRENT_BRANCH; then
		git branch $CURRENT_BRANCH
	fi

	git checkout $CURRENT_BRANCH

	if git ls-remote --heads --exit-code origin $CURRENT_BRANCH; then
		echo "Branch exists on remote, pulling latest changes"
		git pull origin $CURRENT_BRANCH
	fi

	leaveDir
}

function commitAndPush {
	enterDir $1

	git add -N .

	if ! git diff --exit-code > /dev/null; then
		git add .
		git commit -m "Auto Creation of Proto"
		git push origin HEAD
	else
		echo "No changes detected for $1"
	fi

	leaveDir
}

function publish {
	service=$1
	language=$2

	repoName="protorepo-$service-$language"
	repoUrl="${GIT_URL_BASE}/${repoName}.git"

	echo "Publishing $repoName to $repoUrl"
	rm -rf $REPO_PATH/$repoName
	git clone $repoUrl $REPO_PATH/$repoName
	setupBranch $REPO_PATH/$repoName
	cp -R $BUILD_PATH/$service/$language/* $REPO_PATH/$repoName/
	commitAndPush $REPO_PATH/$repoName
}

function publishAll {
	mkdir -p $REPO_PATH

	for serviceDir in ${BUILD_PATH}/*/; do
		for langDir in ${serviceDir}/*/; do
			publish $(basename $serviceDir) $(basename $langDir)
		done
	done
}

publishAll
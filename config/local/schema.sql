-- schema.sql
-- Since we might run the import many times we'll drop if exists
DROP DATABASE IF EXISTS hippos;

CREATE DATABASE hippos;

DROP DATABASE IF EXISTS hippos_test;

CREATE DATABASE hippos_test;

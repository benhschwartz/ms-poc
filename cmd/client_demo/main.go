package main

import (
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"flag"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
)

var (
	addr = flag.String("addr", "localhost:9000", "The server address in the format of host:port")
)

func main() {

	flag.Parse()

	var conn *grpc.ClientConn
	conn, err := grpc.Dial(
		*addr,
		grpc.WithInsecure(),
	)
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()

	c := zoo.NewZooClient(conn)

	// add
	created, err := c.CreateHippo(context.Background(), &zoo.CreateHippoRequest{
		Hippo: &zoo.Hippo{Name: "Bob Loblaw", Weight: 3000},
	})
	if err != nil {
		log.Fatalf("add - error adding zoo: %s", err)
	}
	fmt.Printf("Hippo Added: %v\n", created)

	// get
	found, err := c.GetHippo(context.Background(), &zoo.GetHippoRequest{Id: created.GetId()})
	if err != nil {
		log.Fatalf("get - error getting zoo: %s", err)
	}
	fmt.Printf("Hippo found: %v\n", found)

}

package main

import (
	app "bitbucket.org/benhschwartz/ms-poc/internal/app"
	"bitbucket.org/benhschwartz/ms-poc/internal/dal"
	"bitbucket.org/benhschwartz/ms-poc/pkg/services"
	zoo "bitbucket.org/benhschwartz/protorepo-zoo-go"
	"database/sql"
	"fmt"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/signal"
)

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("APP_ID", "zoo")
	viper.SetDefault("GRPC_PORT", "9000")
	viper.SetDefault("PROMETHEUS_PORT", "5200")
	viper.SetDefault("HEALTH_CHECK_PORT", "5000")
}

func main() {
	appId := viper.GetString("APP_ID")
	grpcPort := viper.GetInt32("GRPC_PORT")
	prometheusPort := viper.GetInt32("PROMETHEUS_PORT")
	healthCheckPort := viper.GetInt32("HEALTH_CHECK_PORT")
	dbDsn := viper.GetString("DATABASE_DSN")

	// Wire deps
	db, err := sql.Open("pgx", dbDsn)
	if err != nil {
		log.Fatalf("could not connect to the database... %v", err)
	}
	defer db.Close()

	zooDal := &dal.ZooDal{Db: db}

	grpcServer, err := services.CreateStatsEnabledGRPCServer()
	if err != nil {
		log.Fatalf("Failed to create gRPC server: %v", err)
	}
	zoo.RegisterZooServer(grpcServer, app.NewZooServer(zooDal))

	// Start Servers
	go startHealthCheckServer(healthCheckPort)
	go startPrometheusServer(prometheusPort, appId)
	go startGRPCServer(grpcPort, grpcServer)

	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)
	<-stop

	log.Print("Shutting down...")
}

func startHealthCheckServer(port int32) {
	log.Print("Starting HealthCheck server")

	s := services.CreateHealthCheckServer(port)
	if err := s.ListenAndServe(); err != nil {
		log.Fatalf("Failed to run HealthCheck server: %v", err)
	}
}

func startPrometheusServer(port int32, appId string) {
	log.Print("Starting Prometheus server")

	s, err := services.CreatePrometheusServer(port, appId)
	if err != nil {
		log.Fatalf("Failed to create Prometheus server: %v", err)
	}
	if err := s.ListenAndServe(); err != nil {
		log.Fatalf("Failed to run Prometheus server: %v", err)
	}
}

func startGRPCServer(portNumber int32, grpcServer *grpc.Server) {
	log.Print("Starting gRPC server")

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", portNumber))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to run gRPC server: %v", err)
	}
}
